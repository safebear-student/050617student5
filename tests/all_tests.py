import base_test

class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
        """This test ensures that you can log into the Safebear automate website with valid credentials

        Given that I have navigated to the page and clicked Login
        When I enter the correct username and password
        Then I am taken to the Main (Logged In) Page"""

        # Step 1: Click on Login and the Login Page Loads
        assert self.welcomepage.click_login(self.loginpage)

        # Step 2:  Login to webpage and the main page appears
        assert self.loginpage.login(self.mainpage, self.param.user, self.param.passw)

        # Step 3: Log Out of Webpage. Welcome Page Loads
        assert self.mainpage.click_logout(self.welcomepage)
